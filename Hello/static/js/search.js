function search(){
    var namaBuku=document.getElementById("cariBuku").value;
    var namaBukuDiURL="";
    for(var i = 0; i < namaBuku.length; i++){
        if(namaBuku.charAt(i) != " "){
            namaBukuDiURL+=namaBuku.charAt(i);
        }
        else{
            namaBukuDiURL+="+";
        }
    }
    var addAnotherHTMLCode="</br></br>";
    $.ajax({
        url : "https://www.googleapis.com/books/v1/volumes?q="+namaBukuDiURL, success : function(data){
            $("#result").empty();
            addAnotherHTMLCode+="<table bgcolor='white' align='center' style='margin-top: 40px; margin-bottom: 40px; font-size: 24px; width:90%'><tr><td colspan=2 style='font-size:20px'>Search Results</br></br></td></tr><tr><td colspan=2 style='background-color:var(--font-color)'></td></tr>";
            $.each(data.items, function(e, item){
                addAnotherHTMLCode+="<tr>";
                if(item.volumeInfo.imageLinks){
                    if(!item.volumeInfo.imageLinks.smallThumbnail){
                        if(!item.volumeInfo.imageLinks.thumbnail){
                            addAnotherHTMLCode+="<td style='color:red; width:180px;'>No image</td>";
                        }
                        addAnotherHTMLCode+="<td style='width:250px; height:400px;'><img src="+item.volumeInfo.imageLinks.thumbnail+" style='width: 180px;'></td>";
                    }
                    else{
                        addAnotherHTMLCode+="<td style='width:250px; height:400px;'><img src="+item.volumeInfo.imageLinks.smallThumbnail+" style='width: 180px;'></td>";
                    }
                }
                else{
                    addAnotherHTMLCode+="<td>No image</td>";
                }
                addAnotherHTMLCode+="<td style='height:270px;'><p style='font-weight: 700;'>"+item.volumeInfo.title;
                if(item.volumeInfo.subtitle){
                    addAnotherHTMLCode+=": "+item.volumeInfo.subtitle+"</a>";
                }
                addAnotherHTMLCode+="</p>";
                if(!item.volumeInfo.authors){
                    addAnotherHTMLCode+="<p>Author: <a style='color:red'>Unknown</a></p>";
                }
                else{
                    addAnotherHTMLCode+="<p>Author: "+item.volumeInfo.authors+"<br></p>";
                }
                if(!item.volumeInfo.averageRating){
                    addAnotherHTMLCode+="<p>Rating: <a style='color:red'>No rating</a></p>";
                }
                else{
                    addAnotherHTMLCode+="<p>Rating: "+item.volumeInfo.averageRating+"<br></p>";
                }
                addAnotherHTMLCode+="<p><a href='"+item.volumeInfo.infoLink+"' style='text-decoration: none'>More Info</a></p>";
                addAnotherHTMLCode+="</td><tr><td colspan=2 style='background-color:var(--font-color)'></td></tr>";
            });
            addAnotherHTMLCode+="</tr></table>"
            $("#result").append(addAnotherHTMLCode);
        }
    });
}